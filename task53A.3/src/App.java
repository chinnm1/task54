public class App {
    public static void main(String[] args) throws Exception {
        Employee employee1 = new Employee();
        Employee employee2 = new Employee(1, "mai", "nguyen", 10000000);

        System.out.println(employee1.toString());
        System.out.println(employee2.toString());
        System.out.println("ho va ten employee1" + employee1.getFirstName() + employee1.getLastName() + "luong 1 nam"
                + employee1.getAnnnualSalary() + "tang luong" + employee1.raiseSalary(10));
        System.out.println("ho va ten employee1 " + employee2.getFirstName() + employee2.getLastName() + " luong 1 nam "
                + employee2.getAnnnualSalary() + " gtang luong " + employee2.raiseSalary(15));
    }
}

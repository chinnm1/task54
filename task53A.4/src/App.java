public class App {
    public static void main(String[] args) throws Exception {
        InvoiceItem invoiceItem1 = new InvoiceItem();
        InvoiceItem invoiceItem2 = new InvoiceItem("L01", "laptop", 2, 30000000);
        System.out.println(invoiceItem1.toString());
        System.out.println(invoiceItem2.toString());
        System.out.println("Tong gia của invoiceItem1" + invoiceItem1.getTotal());
        System.out.println("Tong gia của invoiceItem2" + invoiceItem2.getTotal());
    }
}

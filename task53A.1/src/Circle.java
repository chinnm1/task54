
public class Circle {
    private double radius = 1.0;

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle() {

    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return Math.pow(this.radius, 2) * Math.PI;

    }

    public double getCircumference() {
        return this.radius * 2 * Math.PI;

    }

    @Override
    public String toString() {
        return "Circle[" + this.radius + "]";
    }

    public static void main(String[] args) {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(3.0);
        System.out.println(circle1.toString());
        System.out.println(circle2.toString());
        System.out.println(circle1.getArea());
        System.out.println(circle1.getCircumference());
        System.out.println(circle2.getArea());
        System.out.println(circle2.getCircumference());
    }

}

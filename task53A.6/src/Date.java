
public class Date {
    private int day;
    private int month;
    private int year;

    public Date(int day, int month, int year) {
        this.month = checkMonth(month);
        this.day = checkDay(day);

        this.year = checkYear(year);
    }

    // validate month
    private int checkMonth(int testMonth) {
        if (testMonth > 0 && testMonth <= 12) // validate month
            return testMonth;
        else // month is invalid
        {
            System.out.printf(
                    "Invalid month (%d) set to 1.", testMonth);
            return 1; // maintain object in consistent state
        } // end else
    } // end method checkMonth

    // validate day
    private int checkDay(int testDay) {
        int daysPerMonth[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        // check if day in range for month
        // check if day in range for month
        if (testDay > 0 && testDay <= daysPerMonth[month]) {
            return testDay;
        }

        // check for leap year
        else if (month == 2 && testDay == 29 && (year % 400 == 0 ||
                (year % 4 == 0 && year % 100 != 0))) {
            return testDay;
        } else {
            System.out.printf("Invalid day (%d) set to 1.", testDay);
            return 1; // maintain object in consistent state
        }

    } // end method checkDay

    // end method checkDay
    // validate year

    private int checkYear(int testYear) {
        if (testYear >= 1990 && testYear <= 9999) {
            return testYear;
        } else {
            System.out.printf(
                    "Invalid month (%d) set to 1.", testYear);
            return 1;
        }
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return String.format("%02d/%d/%d", day, month, year);

    }

}

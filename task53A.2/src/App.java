public class App {
    public static void main(String[] args) throws Exception {
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(3.0f, 2.0f);

        System.out.println(rectangle1.toString());
        System.out.println(rectangle2.toString());

        double dientich1 = rectangle1.getArea();
        double dientich2 = rectangle2.getArea();

        double chivi1 = rectangle1.getPerimerter();
        double chuvi2 = rectangle2.getPerimerter();

        System.out.println("Dien tich cua doi tuong 1" + dientich1 + "chu vi" + chivi1);
        System.out.println("Dien tich cua doi tuong 2" + dientich2 + "chu vi" + chuvi2);
    }
}

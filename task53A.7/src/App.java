public class App {
    public static void main(String[] args) throws Exception {
        Time time1 = new Time(1, 3, 3);
        Time time2 = new Time(11, 20, 20);
        System.out.println(time1.toString());
        System.out.println(time2.toString());
        time1.nexSecond(time1);
        System.out.println(time1.toString());
        time2.previousSecond(time2);
        System.out.println(time2.toString());
    }
}


public class Time {
    private int hour;
    private int minute;
    private int second;

    public Time(int hour, int minute, int second) {
        this.hour = checkHour(hour);
        this.minute = checkMinute(minute);
        this.second = checkSecond(second);
    }

    // validate hour
    private int checkHour(int testHour) {
        if (testHour >= 0 && testHour <= 23) {
            return testHour;
        } else {
            System.out.printf(
                    "Invalid hour (%d) set to 1.", testHour);
            return 1;
        }
    }

    // validate minute
    private int checkMinute(int testMinute) {
        if (testMinute >= 0 && testMinute <= 59) {
            return testMinute;
        } else {
            System.out.printf(
                    "Invalid minute (%d) set to 1.", testMinute);
            return 1;
        }
    }

    // validate second
    private int checkSecond(int testSecond) {
        if (testSecond >= 0 && testSecond <= 59) {
            return testSecond;
        } else {
            System.out.printf(
                    "Invalid second (%d) set to 1.", testSecond);
            return 1;
        }
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSecond() {
        return second;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public Time nexSecond(Time time) {
        time.second = time.second + 1;
        return time;
    }

    public Time previousSecond(Time time) {
        time.second = time.second - 1;
        return time;
    }

    @Override
    public String toString() {
        return String.format("%02d/%02d/%02d", hour, minute, second);

    }

}

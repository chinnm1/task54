public class App {
    public static void main(String[] args) throws Exception {
        Account account1 = new Account("01", "Hoa");
        Account account2 = new Account("02", "Mai", 1000);
        System.out.println(account1.toString());
        System.out.println(account2.toString());
        account1.credit(2000);
        account2.credit(3000);
        System.out.println(account1.toString());
        System.out.println(account2.toString());
        account1.debit(1000);
        account2.debit(5000);
        System.out.println(account1.toString());
        System.out.println(account2.toString());
        System.out.println("stranfer account");
        account1.transferTo(account2, 2000);
        System.out.println(account1.toString());
        System.out.println(account2.toString());
        System.out.println("stranfer account2 to account 1");
        account2.transferTo(account1, 2000);
        System.out.println(account1.toString());
        System.out.println(account2.toString());

    }
}

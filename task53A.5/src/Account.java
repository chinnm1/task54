public class Account {

    private String id;
    private String name;
    private int Balance = 0;

    public Account(String id, String name, int balance) {
        this.id = id;
        this.name = name;
        Balance = balance;
    }

    public Account(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return Balance;
    }

    public int credit(int amount) {
        Balance = Balance + amount;
        return Balance;
    }

    public int debit(int amount) {
        if (amount <= Balance) {
            Balance = Balance - amount;
            return Balance;
        } else {
            System.out.println("Amount exceed balance");
            ;
            return Balance;
        }
    }

    public int transferTo(Account another, int amount) {
        if (amount <= Balance) {
            Balance = Balance - amount;
            another.Balance = another.Balance + amount;
            return Balance;
        } else {
            System.out.println("Amount exceeded balance");
            return Balance;
        }
    }

    @Override
    public String toString() {
        return String.format("id = %s , name = %s", id, name + " balance = " + Balance);
    }

}
